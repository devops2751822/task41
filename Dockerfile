# Use the official Nginx image as a base
FROM nginx:latest

# Install wget
RUN apt-get update && apt-get install -y wget && rm -rf /var/lib/apt/lists/*

# Remove the default Nginx static content
RUN rm -rf /usr/share/nginx/html/*
COPY nginx.conf /etc/nginx/conf.d/default.conf
# Download the custom index.html from S3 and copy it to the Nginx directory
RUN wget -O /usr/share/nginx/html/index.html "https://storage.yandexcloud.net/task41/index.html?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=YCAJECMnmlLXUEjpcCmISMC40%2F20240704%2Fru-central1%2Fs3%2Faws4_request&X-Amz-Date=20240704T092742Z&X-Amz-Expires=72000&X-Amz-Signature=EEFBF8877F5040DAE469F817DED207011C2D617C4D17B3EB1FA8A9582DF32F67&X-Amz-SignedHeaders=host"

# Expose port 80
EXPOSE 80

# Start Nginx
CMD ["nginx", "-g", "daemon off;"]
